import sys
from pathlib import Path

from PySide2.QtWidgets import (QApplication,
                               QListWidget,
                               QFrame,
                               QAbstractItemView,
                               QAction)
from PySide2.QtCore import Qt, QSize, Signal


class CustomListWidget(QListWidget):
    new_file = Signal(str)

    def __init__(self, parent=None, accepted_files=('.zip', '.obj', '.fbx')):
        super(CustomListWidget, self).__init__(parent)

        # what file format to accept during the drag and drop
        self.__accepted_file_types = accepted_files

        self.setAcceptDrops(True)
        self.setFrameShape(QFrame.NoFrame)
        self.setFrameShadow(QFrame.Plain)
        self.setDragDropMode(QAbstractItemView.DropOnly)
        self.setAlternatingRowColors(True)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setIconSize(QSize(40, 40))

        # setup the right-click action on the list widget
        self.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.delete = QAction(self)
        self.delete.setText("Remove")
        self.delete.triggered.connect(self.remove_selected)
        self.addAction(self.delete)

    def remove_selected(self):
        """Remove the selected items from the list widget"""
        selected_items = self.selectedItems()

        for item in reversed(selected_items):
            self.takeItem(self.row(item))

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(Qt.CopyAction)
            event.accept()
            for url in event.mimeData().urls():
                file_path = Path(url.toLocalFile())
                if file_path.suffix.lower() in self.__accepted_file_types:
                    self.new_file.emit(url.toLocalFile())


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = CustomListWidget()
    window.show()

    sys.exit(app.exec_())
