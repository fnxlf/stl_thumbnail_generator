import sys
import re
from pathlib import Path
from importlib import import_module


def find_file_names(file_content: str) -> list:
    """Find the texture file name from the given string

    .. important:: the duplicate file names will be removed

    .. code-block:: python

       >>> mtl_content_01 = '''
       ... newmtl PT_FABRIC_FRONT_5993
       ... map_Kd a111 wSnow_co0.tiff
       ... d 1.000000 1.000000 1.000000
       ... map_Ka  owSnowSnow_co0.tif
       ... s 0.000000 0.000000 0.000000
       ...  0.950000
       ... map_Ka 419WG009_SnowSnowSnow_colorway10.png
       ... '''
       >>> print(find_file_names(mtl_content_01))
       ... ['419WG009_SnowSnowSnow_colorway10.png',
       ...  'owSnowSnow_co0.tif',
       ...  'wSnow_co0.tiff']
       >>> mtl_content_02 = '''
       ... Ns 28.763239
       ... illum 2
       ... d 1.000000
       ... map_Kd ¿©ÀÚ TEEPT3x_4cm_R255_G255_B255.png
       ... map_d ¿©ÀÚ TEEPT3x_4cm_OP.png
       ... map_Kd Artwork_02_R0_G0_B0_DESATURATION.png
       ... map_d Artwork_02_OP.png
       ... '''
       >>> print(find_file_names(mtl_content_02))
       ... ['Artwork_02_OP.png',
       ...  'Artwork_02_R0_G0_B0_DESATURATION.png',
       ...  '¿©ÀÚ TEEPT3x_4cm_OP.png',
       ...  '¿©ÀÚ TEEPT3x_4cm_R255_G255_B255.png']

    :param file_content: content of the mtl file
    :type file_content: str
    :return: list of the
    :rtype: list
    """
    return sorted(
        list(
            set(
                re.findall(
                    r'map_[a-zA-Z0-9]{1,10}\s(.*\.[a-z]{3,5})',
                    file_content
                )
            )
        )
    )


def find_local_resource(file_name: str) -> Path:
    """Find the specified file from this package. The file could be anything
    from the image to a python scripts.

    :param file_name: name of the file to find
    :type file_name: str
    :return: path to the file
    :rtype: Path
    """
    # we need to use "_MEIPASS" which is used for when this app is packaged as
    # .exe file. when we run the .exe file, it unpacks its content into a temp
    # directory which can be access like so
    current_directory = getattr(sys,
                                '_MEIPASS',
                                Path(__file__).parent.resolve())
    if isinstance(current_directory, str):
        current_directory = Path(current_directory)

    parent_directory = current_directory.parent

    result = [_ for _ in current_directory.rglob(f'{file_name}')]
    if not result:
        result = [_ for _ in parent_directory.rglob(f'{file_name}')]

    if not result:
        result = [_ for _ in parent_directory.parent.rglob(f'*{file_name}')]

    return result[0] if result else None


def find_module(module_path):
    """Find the given module from the path and import it

    :param module_path: abs path to module
    :return: imported module
    """
    sys.path.insert(0, Path(module_path).parent.as_posix())

    module = import_module(f'{Path(module_path).stem}')

    return module


def find_images(source: str) -> list:
    """retrieve images from a path, if the path is a directory other was return
    the path itself.

    .. note:: valida files are PNG, TIFF, TIF, JPG, JEPG

    :param source: path to a directory or a file
    :type source: str
    :return: list of images
    :rtype: list
    """
    valid_types = ['.png', '.tiff', '.tif', '.jpg', '.jpeg']
    source = Path(source)

    if source.is_dir():
        source_images = set()
        for extension in valid_types:
            [source_images.add(_.as_posix()) for _ in
             source.glob(f'*{extension}')]
        return list(source_images)

    return [] if source.suffix not in valid_types else [source.as_posix()]
