"""
Utility modules that could be used for any QT UI
"""
from PySide2.QtWidgets import QMessageBox
from PySide2.QtGui import QPalette, QColor, QIcon, QPixmap

from libs.finder import find_local_resource

STYLE = """
QListView::item:selected:hover, QTreeView::item:selected:hover
{
    background: rgb(180, 180, 180);
}
QListView::item:hover, QTreeView::item:hover  
{
    background: rgb(100, 100, 100);
}

QLineEdit:focus, QToolButton:hover
{
    border: 1px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);
}

QScrollBar::add-page:hover, QScrollBar::sub-page:hover
{
    background: rgb(70, 70, 70);
}

"""


def set_style(app):
    """Set the default look and feel of the UI

    :param app: QApplication instance
    :type app: PySide2.QtWidgets.QApplication
    """
    app.setStyle("fusion")

    # Build ourselves a dark palette to assign to the application.
    # This  will take the fusion style and darken it up.
    palette = QPalette()

    palette.setBrush(QPalette.Disabled, QPalette.Button,
                     QColor(80, 80, 80))
    palette.setBrush(QPalette.Disabled, QPalette.Light,
                     QColor(97, 97, 97))
    palette.setBrush(QPalette.Disabled, QPalette.Midlight,
                     QColor(59, 59, 59))
    palette.setBrush(QPalette.Disabled, QPalette.Dark,
                     QColor(37, 37, 37))
    palette.setBrush(QPalette.Disabled, QPalette.Mid,
                     QColor(45, 45, 45))
    palette.setBrush(QPalette.Disabled, QPalette.Base,
                     QColor(42, 42, 42))
    palette.setBrush(QPalette.Disabled, QPalette.Window,
                     QColor(68, 68, 68))
    palette.setBrush(QPalette.Disabled, QPalette.Shadow,
                     QColor(0, 0, 0))
    palette.setBrush(
        QPalette.Disabled,
        QPalette.AlternateBase,
        palette.color(QPalette.Disabled, QPalette.Base).lighter(
            110)
    )
    palette.setBrush(
        QPalette.Disabled,
        QPalette.Text,
        palette.color(QPalette.Disabled, QPalette.Base).lighter(
            250)
    )
    palette.setBrush(
        QPalette.Disabled,
        QPalette.Link,
        palette.color(QPalette.Disabled, QPalette.Base).lighter(
            250)
    )
    palette.setBrush(
        QPalette.Disabled,
        QPalette.LinkVisited,
        palette.color(QPalette.Disabled, QPalette.Base).lighter(
            110)
    )

    palette.setBrush(QPalette.Active, QPalette.WindowText,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Active, QPalette.Button,
                     QColor(75, 75, 75))
    palette.setBrush(QPalette.Active, QPalette.ButtonText,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Active, QPalette.Light,
                     QColor(97, 97, 97))
    palette.setBrush(QPalette.Active, QPalette.Midlight,
                     QColor(59, 59, 59))
    palette.setBrush(QPalette.Active, QPalette.Dark,
                     QColor(37, 37, 37))
    palette.setBrush(QPalette.Active, QPalette.Mid,
                     QColor(45, 45, 45))
    palette.setBrush(QPalette.Active, QPalette.Text,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Active, QPalette.Link,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Active, QPalette.LinkVisited,
                     QColor(97, 97, 97))
    palette.setBrush(QPalette.Active, QPalette.BrightText,
                     QColor(37, 37, 37))
    palette.setBrush(QPalette.Active, QPalette.Base,
                     QColor(42, 42, 42))
    palette.setBrush(QPalette.Active, QPalette.Window,
                     QColor(68, 68, 68))
    palette.setBrush(QPalette.Active, QPalette.Shadow,
                     QColor(0, 0, 0))
    palette.setBrush(
        QPalette.Active,
        QPalette.AlternateBase,
        palette.color(QPalette.Active, QPalette.Base).lighter(110)
    )

    palette.setBrush(QPalette.Inactive, QPalette.WindowText,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Inactive, QPalette.Button,
                     QColor(75, 75, 75))
    palette.setBrush(QPalette.Inactive, QPalette.ButtonText,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Inactive, QPalette.Light,
                     QColor(97, 97, 97))
    palette.setBrush(QPalette.Inactive, QPalette.Midlight,
                     QColor(59, 59, 59))
    palette.setBrush(QPalette.Inactive, QPalette.Dark,
                     QColor(37, 37, 37))
    palette.setBrush(QPalette.Inactive, QPalette.Mid,
                     QColor(45, 45, 45))
    palette.setBrush(QPalette.Inactive, QPalette.Text,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Inactive, QPalette.Link,
                     QColor(200, 200, 200))
    palette.setBrush(QPalette.Inactive, QPalette.LinkVisited,
                     QColor(97, 97, 97))
    palette.setBrush(QPalette.Inactive, QPalette.BrightText,
                     QColor(37, 37, 37))
    palette.setBrush(QPalette.Inactive, QPalette.Base,
                     QColor(42, 42, 42))
    palette.setBrush(QPalette.Inactive, QPalette.Window,
                     QColor(68, 68, 68))
    palette.setBrush(QPalette.Inactive, QPalette.Shadow,
                     QColor(0, 0, 0))
    palette.setBrush(
        QPalette.Inactive,
        QPalette.AlternateBase,
        palette.color(QPalette.Inactive, QPalette.Base).lighter(
            110)
    )

    app.setPalette(palette)

    # Finally, we just need to set the default font size for our widgets
    # deriving from QWidget. This also has the side effect of correcting
    # a couple of styling quirks in the tank dialog header when it's
    # used with the fusion style.
    app.setStyleSheet(
        ".QWidget { font-size: 11px; }"
    )

    app.setStyleSheet(STYLE)


def show_warning(display_text, title, details=None, parent=None):
    """Pop up a window to show a warning message

    :param display_text: Text to display in the message box
    :type display_text: str
    :param title: Title of the pop window
    :type title: str
    :param details: More details to show, such as error log.
    :type details: str
    :param parent: QtWidget
    :type parent: widget that uses this pop window
    """
    message = QMessageBox(parent)
    message.setIcon(QMessageBox.Warning)
    message.setText(display_text)
    message.setWindowTitle(title)
    if details:
        message.setDetailedText(details)

    message.exec_()


def get_icon(icon_name: str) -> QIcon:
    """Get the icon from the resource folder based on the given file name

    :param icon_name: name of the icon
    :type icon_name: str
    :rtype: QIcon
    """
    icon_path = find_local_resource(f'{icon_name}.png')
    if not icon_path:
        return QIcon()

    icon = QIcon()
    icon.addPixmap(
        QPixmap(icon_path.as_posix()),
        QIcon.Normal,
        QIcon.Off
    )
    return icon
