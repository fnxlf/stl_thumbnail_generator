import sys
from pathlib import Path

import bpy

current_directory = getattr(sys, "_MEIPASS", Path(__file__).parent.resolve())
if isinstance(current_directory, str):
    current_directory = Path(current_directory)

sys.path.insert(0, current_directory.parent.as_posix())

from libs.logger import get_logger


def build_scene(stl_path):
    posix_path = Path(stl_path).as_posix()
    logger = get_logger("build_blend_scene")
    bpy.ops.import_mesh.stl(filepath=posix_path)

    geo = [obj for obj in bpy.data.objects if obj.type == "MESH"][0]

    geo.scale[0] = 0.01
    geo.scale[1] = 0.01
    geo.scale[2] = 0.01

    bpy.context.scene.render.resolution_x = 1024
    bpy.context.scene.render.resolution_y = 1024

    bpy.context.scene.camera = bpy.data.objects['Camera']

    # save the scene
    blend_path = Path(stl_path).with_suffix(".blend")
    logger.info(f"saving scene as {blend_path.as_posix()}")

    bpy.ops.wm.save_as_mainfile(filepath=blend_path.as_posix(), compress=True)


if __name__ == '__main__':
    args = [_ for _ in reversed(sys.argv)]
    stl_path = args[0]
    build_scene(stl_path)


