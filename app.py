import subprocess
import sys
import traceback
from pathlib import Path
import shutil

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QMainWindow, QApplication, QListWidgetItem, \
    QMessageBox

from resources.views.stl_thumbnail import Ui_stl_thumb_app
from libs.logger import get_logger
from libs.finder import find_local_resource
from libs.custom_list_widget import CustomListWidget
from libs.qt_utils import set_style


class StlThumbnailApp(QMainWindow):
    def __init__(self, parent=None):
        super(StlThumbnailApp, self).__init__(parent)
        self.ui = Ui_stl_thumb_app()
        self.ui.setupUi(self)

        self.logger = get_logger()
        self.files_list = CustomListWidget(accepted_files=("", ".stl"))
        self.ui.list_layout.addWidget(self.files_list)
        self.__set_info("Ready to start")

        # connect UI signals
        self.ui.gen_button.clicked.connect(self.process_items)
        self.files_list.new_file.connect(self.insert_item)

    def __set_info(self, message, state="default"):
        if state == "done":
            color = "#00ff06"
        elif state == "fail":
            color = "red"
        else:
            color = "#00b2ff"
        self.ui.statusbar.setStyleSheet(f"color: {color}")
        self.ui.statusbar.setText(message)
        qApp.processEvents()

    def insert_item(self, new_file_path: str):
        file_path = Path(new_file_path)

        file_item = QListWidgetItem(file_path.name)
        file_item.setData(Qt.UserRole, file_path)

        self.files_list.addItem(file_item)

        self.__set_info("Waiting to start...")

    def is_blender_available(self):
        """Make sure blender is available"""
        if not shutil.which('blender'):
            message = QMessageBox()
            message.setIcon(QMessageBox.Warning)
            message.setText(
                "I can not locate the Blender executable. "
                "To be able to use the lighting template, you "
                "need to make sure that the Blender is in "
                "your system environment path.",
            )
            message.setWindowTitle("Missing Blender!")
            message.exec_()
        self.__set_info("Missing Blender!", "fail")

    def process_items(self):
        self.is_blender_available()
        for row in reversed(range(self.files_list.count())):
            item = self.files_list.item(row)

            self.__set_info(f"Working on {item.data(Qt.UserRole).as_posix()}")
            item_path: Path = item.data(Qt.UserRole)
            if item_path.suffix == "":
                # process all folder stls
                for stl in item_path.glob("*.stl"):
                    self.render_thumbnail(stl)
            else:
                self.render_thumbnail(item_path)

            self.files_list.takeItem(row)

            render_name = item_path.stem
            render_dict = {
                "0": "Angle",
                "1": "Side",
                "2": "Top"
            }
            for render in item_path.parent.glob(f"{render_name}_*.png"):
                frame = render.stem.split("_")[-1]
                new = render.rename(
                    render.with_name(
                        f"{render_name}_{render_dict[frame]}.png"
                    )
                )
                self.logger.debug(f"renamed {render} to {new}")

        self.__set_info("All Done!", "done")

    def render_thumbnail(self, stl_path: Path):
        if not stl_path.exists():
            self.logger.error(f"Could not find STL path {stl_path}")
            self.__set_info("Error reading STL", "fail")

        blend_path = stl_path.with_suffix(".blend")
        build_blend_script = find_local_resource("libs/build_blend_scene.py")
        base_scene = find_local_resource("lighting_setup.blend")

        args = [
            "blender",
            base_scene.as_posix(),
            "--background",
            "--python",
            build_blend_script.as_posix(),
            stl_path.as_posix()
        ]

        self.logger.info(f"Blender command: {' '.join(args)}")
        try:
            subprocess.call(args, shell=True)
        except Exception as error:
            trace_back = traceback.format_exc()
            error_log = f"{trace_back}\n{error}"
            error_message = "Failed setup default render settings"
            self.logger.error(error_message)
            self.logger.error(error_log)
            return

        if self.ui.render_check.isChecked():
            render_args = [
                "blender",
                "-b",
                blend_path.as_posix(),
                "-E",
                "BLENDER_EEVEE",
                "-o",
                blend_path.with_name(blend_path.stem).as_posix() + "_#",
                "-f",
                "0..2"
            ]
            self.logger.info(f"Blender command: {' '.join(render_args)}")

            try:
                subprocess.call(render_args, shell=True)
            except Exception as error:
                trace_back = traceback.format_exc()
                error_log = f"{trace_back}\n{error}"
                error_message = "Failed setup default render settings"
                self.logger.error(error_message)
                self.logger.error(error_log)
                return


if __name__ == '__main__':
    app = QApplication(sys.argv)
    set_style(app)
    window = StlThumbnailApp()
    window.show()

    sys.exit(app.exec_())
