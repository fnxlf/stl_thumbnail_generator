**STL Thumbnail Generator**

Intended for use by the GD team, this app should automate the generation of STL thumbnails using Blender's EEVEE render engine.