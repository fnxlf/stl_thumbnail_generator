# -*- mode: python ; coding: utf-8 -*-

import importlib
import sys

from pathlib import Path

sys.path.insert(0, Path().absolute().as_posix())
from constants import APP_VERSION

block_cipher = None

options = [ ('v', None, 'Option'), ('W ignore', None, 'OPTION') ]
a = Analysis(['app.py'],
             pathex=['.'],
             binaries=[],
             datas=[
                ('resources\\blender_scenes*', 'resources\\blender_scenes'),
                ('libs\\*.py', 'libs')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name=f'STL Thumbnail Generator {APP_VERSION}',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
