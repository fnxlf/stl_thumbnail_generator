# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'stl_thumbnail.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_stl_thumb_app(object):
    def setupUi(self, stl_thumb_app):
        if not stl_thumb_app.objectName():
            stl_thumb_app.setObjectName(u"stl_thumb_app")
        stl_thumb_app.resize(536, 563)
        self.centralwidget = QWidget(stl_thumb_app)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.wiki = QPushButton(self.centralwidget)
        self.wiki.setObjectName(u"wiki")
        self.wiki.setStyleSheet(u"background-color: black;\n"
"border-radius: 3px;\n"
"text-align: left;\n"
"font-weight: bold;\n"
"color: #0091ff;\n"
"font-size: 30px; ")
        self.wiki.setIconSize(QSize(50, 35))

        self.verticalLayout.addWidget(self.wiki)

        self.list_frame = QFrame(self.centralwidget)
        self.list_frame.setObjectName(u"list_frame")
        self.list_frame.setFrameShape(QFrame.StyledPanel)
        self.list_frame.setFrameShadow(QFrame.Raised)
        self.list_layout = QVBoxLayout(self.list_frame)
        self.list_layout.setSpacing(0)
        self.list_layout.setObjectName(u"list_layout")
        self.list_layout.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout.addWidget(self.list_frame)

        self.render_check = QCheckBox(self.centralwidget)
        self.render_check.setObjectName(u"render_check")

        self.verticalLayout.addWidget(self.render_check)

        self.gen_button = QPushButton(self.centralwidget)
        self.gen_button.setObjectName(u"gen_button")

        self.verticalLayout.addWidget(self.gen_button)

        self.statusbar = QLabel(self.centralwidget)
        self.statusbar.setObjectName(u"statusbar")
        self.statusbar.setLineWidth(0)

        self.verticalLayout.addWidget(self.statusbar)

        stl_thumb_app.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(stl_thumb_app)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 536, 21))
        stl_thumb_app.setMenuBar(self.menubar)

        self.retranslateUi(stl_thumb_app)

        QMetaObject.connectSlotsByName(stl_thumb_app)
    # setupUi

    def retranslateUi(self, stl_thumb_app):
        stl_thumb_app.setWindowTitle(QCoreApplication.translate("stl_thumb_app", u"STL Thumbnail Generator", None))
        self.wiki.setText(QCoreApplication.translate("stl_thumb_app", u" STL Thumbnail Generator", None))
        self.render_check.setText(QCoreApplication.translate("stl_thumb_app", u"Render Locally", None))
        self.gen_button.setText(QCoreApplication.translate("stl_thumb_app", u"Generate Thumbs", None))
        self.statusbar.setText(QCoreApplication.translate("stl_thumb_app", u"Status Bar", None))
    # retranslateUi

